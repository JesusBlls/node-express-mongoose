var Comment = require('../models/comments');

exports.readComments = function(req, res){
  Comment.find({})
  .then(function(comments){
    res.json(comments);
  })
  .catch(function(error){
    res.json(error);
  });
};

exports.readOneComment = function(req, res){
  console.log('Updating comment with ID ' + req.params.id);
  Comment.findById(req.params.id)
  .then(function(comment){
    res.json(comment);
  })
  .catch(function (error) {
    console.error('Error updating comment with ID ' + req.params.id, error);
    res.json(error);
  })
};

exports.createComment = function(req, res){
  var newComment = new Comment(req.body);
  newComment.save()
  .then(function(comment){
    res.json(comment);
  })
  .catch(function(error){
    res.json(error)
  });
};

exports.updateOneComment = function (req, res){
  var id= req.params.id;
  var options = {
    new: true
  };
  Comment.findByIdAndUpdate(id, req.body, options)
  .then(function (comment) {
    res.json(comment);
  })
  .catch(function (error) {
    console.error('Error updating comment with ID ' + req.params.id, error);
    res.json(error);
  })
};

exports.deleteOneComment = function (req, res){
  var id= req.params.id;
  var options = {
    new: true
  };
  Comment.findByIdAndRemove(id)
  .then(function () {
    res.json({});
  })
  .catch(function (error) {
    console.error('Error deleting comment with ID ' + req.params.id, error);
    res.json(error);
  })
};
